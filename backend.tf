terraform{
  required_providers {
    aws = {
      source = "hashicorp/aws"
      version = "> 3.27"
    }
  }
  required_version = ">=0.14.9"
}
provider "aws" {
  profile = "default"
  region = "eu-north-1"
}
resource "aws_instance" "App-vendee-globe" {
  ami = "ami-03d079df2b150af9e"
  instance_type = "t3.micro"
  tags = {
    Name = "app-flask-groupe3"
  }
}
resource "aws_key_pair" "deployer" {
  key_name   = "deployer-key"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDiaONuJVh2JH46Kam8YA3fOMi+7wQ0NXA86zK6NyokRZ46Kb3PtB3bGupWEqN9D85tzOg5tZOJGinzyfEbxIZ7g5lfzeVNuKaY+sb7hE0E0m0vGNjx38PFdpPD79pVZwJVFKp1/KqviDyYcadDW10cTccbwcPMT98UB9vfZ9kBeAUW2e78v5SAAupI8KW5BCHjuZcgpmieMrdZT3BeFdTE7zXveQdNtUiqAa+e3Ym5tqSmHZ1MZvyH6lUdUlsEAIQHbTa+gjrcEePVx9kg6uMEJKG3jZjGWtLj5x9ryWwpk+qiQciJVBejVP+svWjO3XuIYWj4b2oM4WeUmVNMqLAT rsa-key-20211124"
}
terraform {
  backend "http" {
  }
}
